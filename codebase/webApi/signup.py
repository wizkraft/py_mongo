from flask import Blueprint
from codebase.extensions import mongo
from codebase.utility import *
import json
from flask import request
from flask_cors import CORS


signup_page = Blueprint('signup_page', __name__)

#Register New User
@signup_page.route('/register', methods=['POST','PUT'])
def register():
    user_collection = mongo.db.UserRegisterData
    #need to improve validation here
    if user_collection.find({"userName": request.json["userName"]}).count() is 0:
        user_collection.insert(createUserObjectForDataBase(request.json))
        return json.dumps(request.json) + '\n'
    else:
        return "UserName is already taken"
        raise Exception("Email registered")