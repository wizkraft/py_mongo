#utility.py

def parsedCoordinates(jsonObj):
    x_cor = float(jsonObj["latitude"])
    y_cor = float(jsonObj["longitude"])
    return { "x": x_cor, "y": y_cor }

def parseGetUserJson(jsonObj):
    x_cor = float(jsonObj["latitude"])
    y_cor = float(jsonObj["longitude"])
    return [x_cor,y_cor], float(jsonObj["radius"]), jsonObj["bloodGroup"]

def createUserObjectForDataBase(jsonObj):
    return {
                "userName" : jsonObj["userName"],
                "password" : jsonObj["password"],
                "firstName" : jsonObj["firstName"],
                "lastName" : jsonObj["lastName"],
                "phoneNumber" : jsonObj["phoneNumber"],
                "bloodGroup" : jsonObj["bloodGroup"],
                "location" : parsedCoordinates(jsonObj),
                "volunteerToDonate" : jsonObj["volunteerToDonate"],
                "covidStatus" : jsonObj["covidStatus"]
            }

def createChatRegistyObjectForDataBase(jsonObj):
    return {
                "id" : jsonObj["id"],
                "user1" : jsonObj["user1"],
                "user2" : jsonObj["user2"],
            }